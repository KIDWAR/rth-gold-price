//
//  String+Utils.swift
//  RTH Gold Price
//
//  Created by MAC on 8/3/17.
//  Copyright © 2017 KD. All rights reserved.
//

import UIKit

extension String {
    
    // Convert Date String format
    static func stringDateToString(myDate: String, fromFormat: String , toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let date = dateFormatter.date(from:myDate)!
        dateFormatter.dateFormat = toFormat
        let dateString = dateFormatter.string(from:date)
        return dateString
    }
    
    // Convert Date to String
    static func dateToString(myDate: Date, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = toFormat
        let dateString = dateFormatter.string(from: myDate)
        return dateString
    }
    
    // Conver String to Date
    static func stringToDate(dateString: String, fromFormat: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let date = dateFormatter.date(from:dateString)!
        return date
    }
    
    // Get weekday symbol
    static func getWeekdaySymbol(index: Int) -> String {
        let f = DateFormatter()
        
        return f.weekdaySymbols[Calendar.current.component(.weekday, from: Date()) - 1]
    }

}
