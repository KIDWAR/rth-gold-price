//
//  UIImageView+Utils.swift
//  RTH Gold Price
//
//  Created by MAC on 8/3/17.
//  Copyright © 2017 KD. All rights reserved.
//

import UIKit
extension UIImageView{
    
    // Make circle image
    func setRounded(borderWidth: CGFloat, borderColor: UIColor) {
        let radius = self.frame.width / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
    }
}
