//
//  PriceCell.swift
//  RTH Gold Price
//
//  Created by MAC on 8/3/17.
//  Copyright © 2017 KD. All rights reserved.
//

import UIKit

class PriceCell: UITableViewCell {

    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var txtPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
