//
//  MainViewController.swift
//  RTH Gold Price
//
//  Created by MAC on 8/3/17.
//  Copyright © 2017 KD. All rights reserved.
//

import UIKit
import Charts

class MainViewController: UIViewController, OnServerCallback {
    
    @IBOutlet weak var tablePrice: UITableView!
    @IBOutlet weak var txtDayOfWeek: UILabel!
    @IBOutlet weak var txtMonthYear: UILabel!
    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var subDialog: UIView!
    @IBOutlet weak var aboutImage: UIImageView!
    
    //list gold price
    var priceList: [Price] = [Price]()
    
    // table refresh controler
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // set service delegate
        UIViewController.service.serverCallbackDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
        
        if let navigationController = self.navigationController {
            let navigationBar = navigationController.navigationBar
            
            // set navigation bar text color
            navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor(hex: Config.text_color)]
            
            // set navigation bottom boder
            navigationBar.shadowImage = UIColor(hex: Config.border_color).as1ptImage()
            navigationBar.setBackgroundImage(UIColor.white.as1ptImage(), for: .default)
        }
        
        setupAboutDialog()
        
        setupTable()
        
        setupChart()
    }
    
    func setupTable() {
        
        // make table border
        tablePrice.layer.borderWidth = 1;
        tablePrice.layer.borderColor = UIColor(hex: Config.border_color).cgColor
        
        // register cell nib
        tablePrice.register(UINib(nibName: "PriceCell", bundle: nil), forCellReuseIdentifier: "PriceCell")
        
        // init, add refresh control
        refreshControl = UIRefreshControl()
        let attr = [NSForegroundColorAttributeName:UIColor(hex: Config.text_color)]
        refreshControl.attributedTitle = NSAttributedString(string: StringConstants.refresh, attributes: attr)
        refreshControl.tintColor = UIColor(hex: Config.text_color)
        refreshControl.addTarget(self, action: #selector(MainViewController.refresh(sender:)), for: UIControlEvents.valueChanged)
        tablePrice.addSubview(refreshControl)
    }
    
    func setupAboutDialog() {
        subDialog.layer.cornerRadius = 5
        // make about image cricle
        aboutImage.setRounded(borderWidth : 1, borderColor: UIColor(hex: Config.border_color))
        
    }
    
    // init, config line chart
    func setupChart() {
        
        let dataEntries: [ChartDataEntry] = []
        
        let lineChartDataSet = LineChartDataSet(values: dataEntries, label: "Price")
        lineChartDataSet.setColor(UIColor(hex: Config.text_color))
        lineChartDataSet.mode = .cubicBezier
        lineChartDataSet.drawCirclesEnabled = true
        lineChartDataSet.circleColors = [UIColor(hex: Config.text_color)]
        lineChartDataSet.circleHoleColor = UIColor(hex: Config.text_color)
        lineChartDataSet.drawValuesEnabled = false
        lineChartDataSet.lineWidth = 2.0
        lineChartDataSet.circleRadius = 3.0
        lineChartDataSet.drawVerticalHighlightIndicatorEnabled = false
        lineChartDataSet.drawHorizontalHighlightIndicatorEnabled = false
        lineChartView.backgroundColor = UIColor.white
        lineChartView.xAxis.drawGridLinesEnabled = false;
        lineChartView.chartDescription?.text = ""
        lineChartView.dragEnabled = false
        lineChartView.setScaleEnabled(false)
        lineChartView.drawGridBackgroundEnabled = false
        lineChartView.xAxis.enabled = false
        lineChartView.leftAxis.enabled = false
        lineChartView.leftAxis.drawGridLinesEnabled = false
        lineChartView.rightAxis.enabled = false;
        lineChartView.legend.enabled = false;
        
        
        var dataSets = [IChartDataSet]()
        dataSets.append(lineChartDataSet)
        let lineChartData = LineChartData(dataSets: dataSets)
        lineChartView.data = lineChartData
    }
    
    func loadData() {
        
        // set calendar label
        let calendar = Calendar.current
        let now = Date()
        let weekday =  calendar.component(.weekdayOrdinal, from: now)
        
        txtDate.text = String.dateToString(myDate: Date(), toFormat: "dd")
        txtMonthYear.text = String.dateToString(myDate: Date(), toFormat: "MMMM yyyy")
        txtDayOfWeek.text = String.getWeekdaySymbol(index: weekday)
        
        // get gold price data and refresh table
        refresh(sender: self.refreshControl)
    }
    
    func refresh(sender:AnyObject) {
        
        // show table loadding
        refreshControl.beginRefreshing()
        
        // get gold price
        UIViewController.service.getPrice()
    }
    
    // load data to chart
    func loadChart() {
        let lineChartDataSet = lineChartView.data?.dataSets[0] as! LineChartDataSet
        
        // remove old entry
        lineChartDataSet.values.removeAll()
        
        // append new entry
        for i in 0..<priceList.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: Double(priceList[i].amount)!)
            lineChartDataSet.values.append(dataEntry)
        }
        
        // notify change
        lineChartView.data?.notifyDataChanged()
        lineChartView.notifyDataSetChanged()
    }
    
    // on request server fail
    func onServerCallbackReturnFail(action: String, message: NSError) {
        print("\(message)");
        showToast(message: "\(message.localizedDescription)")
        refreshControl.endRefreshing()
    }
    
    // on request server success
    func onServerCallback(action: String, data : String) {
        // serializable json string to list price
        priceList = [Price](json: data)
        
        // reload table, chart
        tablePrice.reloadData()
        loadChart()
        refreshControl.endRefreshing()
    }
    
    // close about dialog
    @IBAction func actionCloseDialog(_ sender: Any) {
        dialogView.isHidden = true
    }
    
    // open dialog
    @IBAction func actionAbout(_ sender: Any) {
        dialogView.isHidden = false
    }
 
}

// table datasource, delegate
extension MainViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return priceList.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create cell
        let cell : PriceCell = (tableView.dequeueReusableCell(withIdentifier: "PriceCell", for: indexPath) as? PriceCell)!
        
        // set cell data
        let price = priceList[indexPath.row]
        cell.txtDate.text = String.stringDateToString(myDate: price.date, fromFormat: "dd-MM-yyyy", toFormat: "dd MMMM yyyy")
        cell.txtPrice.text = "$\(price.amount)"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

