//
//  Config.swift
//  RTH Gold Price
//
//  Created by MAC on 8/3/17.
//  Copyright © 2017 KD. All rights reserved.
//

import UIKit

struct StringConstants {
    static let refresh: String = "Pull to refresh"
}

struct Config {
    static let host_name: String = "https://rth-recruitment.herokuapp.com/api"
    static let token: String = "76524a53ee60602ac3528f38"
    static let headers = [
        "Content-Type": "application/x-www-form-urlencoded",
        "X-App-Token" : token
    ]
    static let get_price_url = "/prices/chart_data"
    static let get_price_name = "get_price"
    static let border_color = "9DCDEC"
    static let text_color = "0F85D1"
}
