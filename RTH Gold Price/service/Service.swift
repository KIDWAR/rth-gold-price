//
//  Service.swift
//  RTH Gold Price
//
//  Created by MAC on 8/3/17.
//  Copyright © 2017 KD. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

// Server call back
protocol OnServerCallback {
    func onServerCallback(action: String, data : String)
    func onServerCallbackReturnFail(action: String, message : NSError)
}

class Service : NSObject {
    
    var serverCallbackDelegate : OnServerCallback!
    
    override init() {
        //TODO
    }
    
    // get list gold price from server
    func getPrice() {
        
        let parameters: Parameters = [:]
        sentRequest(method: .get, parameters: parameters, action: Config.get_price_name, api: Config.get_price_url)
    }

    // set request to server
    func sentRequest(method : HTTPMethod,parameters : Parameters, action : String, api : String) {
        
        
        let url = Config.host_name + api
        Alamofire.request(url, method : method, encoding: URLEncoding.default, headers: Config.headers).responseString(encoding: String.Encoding.utf8) {
            response in
            switch response.result {
            case .success:
                print("Response String: \(String(describing: response.result.value))")
                
                let jsonString = response.result.value
                
                self.serverCallbackDelegate.onServerCallback(action: action, data: jsonString!)
                break
            case .failure(let error):
                self.serverCallbackDelegate.onServerCallbackReturnFail(action: action, message: error as NSError)
                break
            }
        }
    }
}
